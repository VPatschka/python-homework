eval $(minikube docker-env)

docker build -t generator ./1-generator/
docker build -t calculator ./2-calculator/
docker build -t sorter ./3-sorter/

kubectl apply -f zookeeper
kubectl apply -f kafka-broker

kubectl apply -f 1-generator
kubectl apply -f 2-calculator
kubectl apply -f 3-sorter
