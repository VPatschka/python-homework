import os
import json
import base64
from time import sleep
from kafka import KafkaProducer


def send_images():
    producer = KafkaProducer(bootstrap_servers='kafka')
    print('Sending images')

    image_directory = 'images'
    image_names = os.listdir(image_directory)
    for image_name in image_names:
        print(image_name)
        image_path = image_directory + '/' + image_name
        with open(image_path, 'rb') as image_file:
            image_bytes = image_file.read()
            base64_image = base64.b64encode(image_bytes).decode('utf-8')
            data = json.JSONEncoder().encode({
                'name': image_name,
                'data': base64_image
            })
            producer.send(topic, data.encode('utf-8'))

    producer.flush()


topic = 'images'

while True:
    send_images()
    sleep(30)
