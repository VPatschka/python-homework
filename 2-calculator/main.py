from kafka import KafkaConsumer, KafkaProducer
from PIL import Image
import numpy as np
import io
import json
import base64

topic_consumer = 'images'
topic_producer = 'color_averages'

consumer = KafkaConsumer(topic_consumer, bootstrap_servers='kafka')
producer = KafkaProducer(bootstrap_servers='kafka')
json_decoder = json.JSONDecoder()
json_encode = json.JSONEncoder()

print('Listening')

for record in consumer:
    value = json_decoder.decode(record.value.decode('utf-8'))

    image_name = value['name']
    image_data = base64.b64decode(value['data'].encode('utf-8'))
    image = Image.open(io.BytesIO(image_data))
    color_average = np.mean(np.array(image), axis=(0, 1))
    red, green, blue = color_average

    print(image_name)
    print(color_average)
    print()

    data = json_encode.encode({
        'name': value['name'],
        'data': value['data'],
        'red': red,
        'green': green,
        'blue': blue
    })
    producer.send(topic_producer, data.encode('utf-8'))