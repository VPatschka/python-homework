from kafka import KafkaConsumer
import os
import json
import base64

topic_consumer = 'color_averages'

directories = ['blue', 'green', 'red']
for directory in directories:
    if not os.path.isdir(directory):
        os.mkdir(directory)

consumer = KafkaConsumer(topic_consumer, bootstrap_servers='kafka')
json_decoder = json.JSONDecoder()

print('Listening')

for record in consumer:
    value = json_decoder.decode(record.value.decode('utf-8'))

    image_name = value['name']
    image_data = base64.b64decode(value['data'].encode('utf-8'))
    red = value['red']
    green = value['green']
    blue = value['blue']

    maxColor = max(red, green, blue)
    if maxColor is red:
        directory = 'red'
    elif maxColor is green:
        directory = 'green'
    elif maxColor is blue:
        directory = 'blue'

    print(image_name + ' - ' + directory)

    with open(directory + '/' + image_name, 'wb') as image_file:
        image_file.write(image_data)
